import React, { Component } from 'react';
import api from '../../services/api';
import {Link} from 'react-router-dom'; //Redireciona o usuário para a segunda rota

import './styles.css';

export default class Main extends Component{
    //Variável de estado, é sempre um objeto. Consigo armazenar as variáveis que quisermos..
    //
    state = {
        products: [], //Variável do tipo array
        productInfo: [],
        page: 1
    }



    //Executa uma ação logo que o componente é exibido na tela
    componentDidMount(){
        this.loadProducts();
    }

    //Quando é função nossa, é preciso criar desse modo, e não pelo método de classe
    loadProducts = async (page = 1)  => {
        const response = await api.get(`/products?page=${page}`);

        //Mostra no console o retorno da API
        //console.log(response.data.docs);

        const { docs, ...productInfo } = response.data;

        //Preenchendo a variável products do state
        this.setState({ products: docs, productInfo, page });

    }

    //Criando funções dos botões PREV e NEXT
    prevPage = () =>{
        const { page, productInfo} = this.state;

        if(page == 1) return;

        const pageNumber = page -1;   

        this.loadProducts(pageNumber);

    }

    nextPage = () =>{
        const { page, productInfo} = this.state;
        
        if(page == productInfo.pages) return;

        const pageNumber = page + 1;

        this.loadProducts(pageNumber);
    }


    //Sempre que tivermos uma variável no State, o render vai ficar escutando a alteração da variável
    //E sempre que alguma variável é alterada, o render executa de novo, trazendo em tela as alterações
    render() {
        //return <h1>Contagem de produtos: { this.state.products.length}</h1>;
        //Busca a variável products do STATE
        const { products, page, productInfo } = this.state;

        return (
            <div className="product-list">
                { products.map(product => (
                    //React pede pra toda vez que é realizado um map, que seja adicionado logo depois, uma key para identificação única para cada item mapeado
                    //<h2 key={product._id} >{product.title}</h2>   
                    
                    <article key={product._id}>
                        <strong>{product.title}</strong>
                        <p>{product.description}</p>
                        <Link to={`/products/${product._id}`}>Acessar</Link>
                    </article>
                    
                ))}

                <div className="actions">
                    <button disabled={page == 1} onClick={this.prevPage}>Anterior</button>
                    <button disabled={page == productInfo.pages} onClick={this.nextPage}>Próxima</button>
                </div>

            </div>
        );
    }

}