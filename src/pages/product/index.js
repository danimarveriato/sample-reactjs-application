import React, { Component } from 'react';
import api from '../../services/api';

import './styles.css';

export default class Product extends Component{

    state = {
        product: {},
    };

    async componentDidMount() {
        //Pegando os parâmetros que vem da rota
        const { id } = this.props.match.params;
        //Busca o id da API
        const response = await api.get(`/products/${id}`);
        //Joga os dados do produto para a variável do state
        this.setState({ product: response.data });
    }

    render() {
        //Pega o produto do State
        const { product } = this.state;

        return (
            <div className="product-info">
                <h1>{product.title}</h1>
                <p>{product.description}</p>

                <p>
                    URL: <a href={product.url}>{product.url}</a>
                </p>

            </div>
        )
    }

}
