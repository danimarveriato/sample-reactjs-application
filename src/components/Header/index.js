import React, { Component } from 'react';
import "./styles.css";


//Forma simplificada de criar uma classe com um retorno de conteúdo html..
const Header = () => (
    <header id="main-header">JS Junt</header>
);


//Segunda forma de criar a classe com retorno de conteúdo html..

//class Header1 extends Component {
//    render(){
//        return <h1>Hello</h1>
//    }
//}


export default Header;