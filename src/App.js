import React from 'react';
import Header from './components/Header';
import "./styles.css"; //Importação do arquivo da raiz da pasta SRC
import Main from './pages/main';
import Routes from './routes';


const App = () => (
  <div className="App">
      <Header />
      <Routes />
    </div>

);

export default App;
